import React from "react";
import "./App.css";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Header from "./Components/Header/Header";
import Sidebar from "./Components/Sidebar/Sidebar";
import styled from "styled-components";
import Chat from "./Components/Chat/Chat";
import { useAuthState } from "react-firebase-hooks/auth";
import { auth } from "./firebase";
import Login from "./Components/Login/Login";
import Spinner from 'react-spinkit'

function App() {
  const [user, loading] = useAuthState(auth);
  
  if (loading) {
    return (
      <AppLoading>
        <AppLoadingContents>
          <img
            src="https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fwebdesigntips.blog%2Fwp-content%2Fuploads%2F2019%2F02%2FSlack-sparks-further-outrage-with-tweak-to-new-logo.jpg&f=1&nofb=1"
            alt=""
          />
        <Spinner
         name='ball-spin-fade-loader'
         color='purple'
         fadeIn='none'
        />  

        </AppLoadingContents>
      </AppLoading>
    );
  }

  return (
    <div className="App">
      <Router>
        {!user ? (
          <Login />
        ) : (
          <>
            <Header />
            <AppBody>
              <Sidebar />
              <Switch>
                <Route path="/" exact>
                  <Chat />
                </Route>
              </Switch>
            </AppBody>
          </>
        )}
      </Router>
    </div>
  );
}

export default App;

const AppBody = styled.div`
  display: flex;
  height: 100vh;
`;

const AppLoading = styled.div`
 display: grid;
 place-items: center;
 height: 100vh;
 width: 100%;
`;

const AppLoadingContents = styled.div`
  text-align: center;
  padding-bottom: 100px;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;

  > img {
    height: 100px;
    padding: 20px;
    margin-bottom: 40px;
  }
`;