import firebase from 'firebase'

const firebaseConfig = {
  apiKey: "AIzaSyBC233NJscCRNYgrCY1OPF8Mfjo0mO_C_4",
  authDomain: "slack-clone-5cf34.firebaseapp.com",
  projectId: "slack-clone-5cf34",
  storageBucket: "slack-clone-5cf34.appspot.com",
  messagingSenderId: "183860778177",
  appId: "1:183860778177:web:f559cf9dd699a3b91bb040"
};
const firebaseApp = firebase.initializeApp(firebaseConfig);

const db = firebaseApp.firestore();
const auth = firebase.auth();
const provider = new firebase.auth.GoogleAuthProvider();

export {auth, provider, db}