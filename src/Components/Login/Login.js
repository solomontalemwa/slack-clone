import React from "react";
import styled from "styled-components";
import { Button } from "@material-ui/core";
import { auth, provider } from "../../firebase";

function Login() {
  const signin = (e) => {
    e.preventDefault();
    auth.signInWithPopup(provider).catch((error) => alert(error.message));
  };
  return (
    <LoginContainer>
      <LoginInnerContainer>
        <img
          src="https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fwebdesigntips.blog%2Fwp-content%2Fuploads%2F2019%2F02%2FSlack-sparks-further-outrage-with-tweak-to-new-logo.jpg&f=1&nofb=1"
          alt=""
        />
        <h1>Sign in to join the community!</h1>
        <p>solowithslack.com</p>

        <Button onClick={signin}>Sign in with Google</Button>
      </LoginInnerContainer>
    </LoginContainer>
  );
}

export default Login;

const LoginContainer = styled.div`
  background-color: #f8f8f8;
  height: 100vh;
  display: grid;
  place-items: center;
`;

const LoginInnerContainer = styled.div`
  padding: 100px;
  text-align: center;
  background-color: white;
  border-radius: 40px;
  box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.24);
  > img {
    height: 100px;
    object-fit: contain;
    margin-bottom: 40px;
  }

  > button {
    margin-top: 50px;
    text-transform: inherit !important;
    background-color: #0a8d48 !important;
    color: white;
  }
`;
